#+TITLE: CatiROC

This project implements a vhdl controller interface to the [[https://portail.polytechnique.edu/omega/en/products/products-presentation/catiroc][catiroc]] asic from the
[[https://portail.polytechnique.edu/omega/fr][Omega]] group. It follows indications given in the asic datasheet.

Details about the implementation are given in the project [[https://ip-vhdl.gitlab.io/catiroc][pages]].

* Contents

=catiroc.core= :: [[https://fusesoc.readthedocs.io/en/master/index.html][fusesoc]] core file

 - =ip= :: ancillary ip blocs, to be used in simulation and implementation

   - =ip/p1_counter= :: example peripheral =p1_counter.vhd=

   - =ip/_= :: several different fifo cores

 - =src= :: rtl source code

   - =src/ip= :: ip source code, along with its package defining custom types

   - =src/impl= :: implementation, providing an example of instantiation and use

 - =sim/runs/catiroc.cocotb= :: simulation using cocotb

   Includes =catiroc_tb.py= [[https://cocotb.readthedocs.io/en/latest/introduction.html][cocotb]] testbench file, a [[https://gitlab.com/ip-vhdl/catiroc/-/blob/master/sim/runs/catiroc.cocotb/Makefile][Makefile]] to run it and a
   =wave.gtkw= to observe simulation results with help of [[http://gtkwave.sourceforge.net/][gtkwave]]. Additionally, a
   [[https://gitlab.com/ip-vhdl/catiroc/-/blob/master/sim/runs/catiroc.cocotb/testme.sh][testme.sh]] script file is provided too to ease running tests.

 - =hdl= :: wrapper hdl file =catiroc_tb.vhd= necessary for type conversion

 - =prj_vivado19.2= :: xilinx vivado related files

   Includes =catiroc.xpr= project file, as well as a =run_all.sh= and =run_all.tcl=
   build files.

* Use

* Simulate

** Cocotb

Simulation instructions are based on that of the [[https://gitlab.com/ip-vhdl/eda-common][eda-common]] repository, used here.

*Contents*

Wrapper =catiroc_tb.vhd= contains an instantiation of the =catiroc= ip. It provides
type conversion, so that the test bench may access the design using standard
types.

The =catiroc_tb.py= [[https://cocotb.readthedocs.io/en/latest/introduction.html][cocotb]] testbench file includes a single test =catiroc_test=.

Following subtests are performed here:

 - driver / monitor :: inject known data randomly over N parallel fifos; capture
   this data, checking its validity

All low level transactions are checked, and all sub tests are verified at
simulation run time.

*Run*

=sim/runs/catiroc.cocotb= directory includes a [[https://gitlab.com/ip-vhdl/catiroc/-/blob/master/sim/runs/ft2232h.cocotb/Makefile][Makefile]], so just

#+begin_src sh
  cd sim/runs/catiroc.cocotb
  make EDA_COMMON_DIR=/tmp/eda-common G_NB_ASICS=1
#+end_src

to run the [[file:pics/sim.gif][simulation]], where =G_NB_ASICS= is the number of asics present, and
=eda-common= may be obtained as follows

#+begin_src sh
  git clone https://gitlab.com/csantosb/eda-common /tmp/eda-common
#+end_src

Debugging informations may be obtained with

#+begin_src sh
  make EDA_COMMON_DIR=/tmp/eda-common G_NB_ASICS=1 DEBUG=1
#+end_src

For exampes refer to [[https://gitlab.com/ip-vhdl/catiroc/-/blob/master/sim/runs/catiroc.cocotb/testme.sh][testme.sh]] script.

Note that [[https://cocotb.readthedocs.io/en/latest/introduction.html][cocotb]] simulation uses [[https://ghdl.readthedocs.io/en/latest/about.html][ghdl]] as a backend. Pre-compiled simulation libs are to be
found under

#+begin_src sh
  $HOME/CompiledLibraries
#+end_src

so create a simlink if necessary. Finally, simulation will provide the resulting
[[file:pics/gtkwave.png][waveform]] (note that =wave.gtkw= is only valid for =G_NB_ASICS= = 8) when =DEBUG=1=
even if not necessary to validate the tests.

[[pics/gtkwave.small.png]]

* Implement


** Vivado (manually)

Create a project. Include these filesets

#+begin_src sh
  []
#+end_src

as defined in =catiroc.core= file. Then, proceed as usual.

** Vivado (provided files)

Just run

#+begin_src sh
  cd prj_vivado19.2
  ./run_all.sh
#+end_src

** Vivado (fusesoc)

#+begin_src sh
  fusesoc --cores-root $ThisProjectRootDir run --no-export --target=implement catiroc
#+end_src

* License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the [[http://www.gnu.org/licenses/gpl.txt][GNU General Public License]]
along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
