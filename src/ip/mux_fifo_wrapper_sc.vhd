-- * CatiROC usb interface
--
--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...
--
-- The goal of this module is to act as a 8 to 1 multiplexor. It takes data from
-- “nb_asics” fifos, each of “input_bus_width_bits” width data buses, and
-- stores it into a local fifo. The later is supposed to be read by the usb manager
-- interface.
--
-- The generics provide some flexibility here, so that this module is assumed to
-- perform regardless of the previous stage. For example, it may be used to move
-- event (128 bits) and slow control (8 bits) data from a previous state of catiroc
-- managers.
--
-- * Header
--
-- * Libraries

library IEEE;

-- * Packages

use ieee.std_logic_1164.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;
use work.edacom_pkg.all;

-- * Entity

-- We take “input_bus_width_bits” bits incoming data from “nb_asics” asic managers,
-- and we store all of this in a “input_bus_width_bits” bits data fifo. The fifo is
-- to be readout to the usb.

entity mux_fifo_wrapper_sc is
  generic (g_enabled_channels   : std_logic_vector     := X"FF";
           g_extend             : natural range 0 to 1 := 0);
  port (global_reset      : in    std_logic;
        -- input interface
        fifo_rd_if           : inout t_fifo_rd_if_array;
        -- fifo_if           : inout t_fifo_if;
        -- if to usb manager
        p_if         : inout rc_peripheral_if_p3
        -- fifo_read_clk     : in    std_logic;
        -- fifo_read_dataout : out   std_logic_vector;
        -- fifo_read_rd      : in    std_logic;
        -- fifo_read_empty   : out   std_logic
        );
end entity mux_fifo_wrapper_sc;

-- * Architecture

architecture Behavioral of mux_fifo_wrapper_sc is

  -- ** Signals

  -- TODO: improve this, they’re the same ! cast ?
  -- Here, I need to map fields of two identical types
  -- Solution would be to use generic types, but they’re not yet supported

  -- signal fifo_if_in : t_mux_fifo_if(data(fifo_if'range)(fifo_if(0).data'range),
  --                                   empty(fifo_if'range),
  --                                   rd(fifo_if'range));

  -- signal fifo_if_in : t_mux_fifo_if(data(fifo_if(0).data'length*fifo_if'length-1 downto 0),
  --                                   empty(fifo_if'range),
  --                                   rd(fifo_if'range));

  -- signal fifo_if_in : t_mux_fifo_if(data(fifo_if.data'range)(fifo_if.data(0)'range),
  --                                   empty(fifo_if.empty'range),
  --                                   rd(fifo_if.rd'range));

  signal dataout     : std_logic_vector(fifo_rd_if(0).data'length+8*g_extend-1 downto 0) := (others => '0');
  signal wr_en, full : std_logic                                                      := '0';

begin

  -- ** Multiplexor

  -- TODO: improve this, they’re the same ! cast ?
  -- Here, I need to map fields of two identical types
  -- Solution would be to use generic types, but they’re not yet supported

  -- u_type_convert : for i in fifo_if'range generate
  --   -- fifo_if_in.data(fifo_if(0).data'length*(i+1)-1 downto fifo_if(0).data'length*i) <= fifo_if(i).data;
  --   fifo_if_in.data(i)  <= fifo_if(i).data;
  --   fifo_if_in.empty(i) <= fifo_if(i).empty;
  --   fifo_if(i).rd       <= fifo_if_in.rd(i);
  --   fifo_if(i).clk      <= fifo_if_in.clk;
  -- end generate u_type_convert;

  -- u_type_convert : for i in fifo_if.data'range generate
  --   fifo_if_in.data(i) <= fifo_if.data(i);
  -- end generate u_type_convert;

  -- fifo_if_in.empty <= fifo_if.empty;
  -- fifo_if.rd       <= fifo_if_in.rd;
  -- fifo_if.clk      <= fifo_if_in.clk;

  u_mux_fifo : entity work.mux_fifo
    generic map (g_enabled_channels => g_enabled_channels,
                 g_extend           => g_extend)
    port map (rst        => global_reset,
              clk        => p_if.clk,
              fifo_rd_if => fifo_rd_if,
              -- out if
              dataout    => dataout,
              wr_en      => wr_en,
              full       => full);

  -- ** Fifo

  -- U_fifo : if fifo_if.data(0)'length = 8 generate
  U_fifo : if fifo_rd_if(0).data'length = 8 generate

  U_fifo_slowcontrol : entity work.fifo_8
      port map (rst    => global_reset,
                -- incoming data from nb_asics ASICS
                -- wr_clk => fifo_if_in.clk,
                wr_clk => p_if.clk,
                din    => dataout,
                wr_en  => wr_en,
                full   => full,
                -- if to usb manager
                rd_clk => p_if.clk,
                dout   => p_if.data,
                rd_en  => p_if.rd,
                empty  => p_if.empty);

  -- elsif fifo_if.data(0)'length = 64 generate
  elsif fifo_rd_if(0).data'length = 64 generate

    U_fifo_capture : entity work.fifo_64
      port map (rst    => global_reset,
                -- incoming data from nb_asics ASICS
                wr_clk => p_if.clk,
                din    => dataout,
                wr_en  => wr_en,
                full   => full,
                -- if to usb manager
                rd_clk => p_if.clk,
                dout   => p_if.data,
                rd_en  => p_if.rd,
                empty  => p_if.empty);

  end generate;

end architecture Behavioral;
