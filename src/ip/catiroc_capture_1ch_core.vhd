-- * catiroc_capture_1ch_core

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;

-- * Entity

entity catiroc_capture_1ch_core is
  generic (output_nb     : std_logic             := '0';
           lenght_strobe : natural range 1 to 63 := 4);  -- 1 for channels from 8 to 15, otherwise 0
  port (rst               : in  std_logic;
        NewEvent          : out std_logic;
        --cati
        clk_80MHz         : in  std_logic;  -- clk out @ 80 MHz
        cati_dataout      : in  std_logic;
        NewEventByChannel : out std_logic_vector(15 downto 0);
        cati_strobe       : in  std_logic;
        nbBitselect       : in  std_logic;  -- A 1 on prends 21bit de G Q et T sinon 11 juste G + Q
        decode_gray       : in  std_logic;  -- Set to 1 to decode to binary
        ---periferal
        fifo_dataout      : out std_logic_vector(63 downto 0);
        fifo_rd           : in  std_logic;
        fifo_empty        : out std_logic);
end entity catiroc_capture_1ch_core;

-- * Architecture

architecture Behavioral of catiroc_capture_1ch_core is

  type dataout_fifo_state_type2 is (standby, write_tempo);

  -- ** Signals

  signal Frame_select                          : std_logic                                  := '0';
  signal data_in                               : std_logic_vector (63 downto 0)             := (others => '0');
  signal q1, q2                                : std_logic_vector (47 downto 0)             := (others => '0');
  signal Fifo_data_in1, Fifo_data_in1_f1       : std_logic_vector (31 downto 0)             := (others => '0');
  signal Fifo_data_in1_f1_del                  : std_logic_vector (47 downto 0)             := (others => '0');
  signal Fifo_data_in2                         : std_logic_vector (31 downto 0)             := (others => '0');
  signal Fifo_data_in2_f2                      : std_logic_vector (47 downto 0)             := (others => '0');
  signal Fifo_data_out_frames                  : std_logic_vector (63 downto 0)             := (others => '0');
  signal Fifo_rden_frames                      : std_logic                                  := '0';
  signal Fifo_wreq                             : std_logic                                  := '0';
  signal Fifo_wreq_frame1, Fifo_wreq_frame1_f1 : std_logic                                  := '0';
  signal Fifo_wreq_frame1_f1_del               : std_logic                                  := '0';
  signal Fifo_wreq_frame2, Fifo_wreq_frame2_f2 : std_logic                                  := '0';
  signal Fifo_full                             : std_logic                                  := '0';
  signal Fifo_empty_frame2                     : std_logic                                  := '0';
  signal Frame_bitContor                       : unsigned (4 downto 0)                      := (others => '0');
  signal strobe                                : std_logic_vector(lenght_strobe-1 downto 0) := (others => '1');
  signal dataout                               : std_logic_vector(lenght_strobe-1 downto 0) := (others => '1');
  signal dataout_uniFramefifo_state            : dataout_fifo_state_type2;
  signal change_frame                          : std_logic                                  := '0';

  -- *** Event counter signals

  -- Each item in =EventCounters= represents the number of event seen from
  --    the output of catiroc. =ChannelNb= represents the current event channel
  --    number. These are used [[*Event counter management][below]].

  type EventCounterType is array(0 to 15) of unsigned(10 downto 0);
  signal EventCounters : EventCounterType     := (others => (others => '0'));
  signal ChannelNb     : unsigned(3 downto 0) := (others => '0');

begin

  -- ** Generic

  -- ** Falling edge detection

-- Detect falling edge on strobe from catiroc
-- Each falling edge swaps the 'Frame_select' flag
--  - 'Frame_select' = 1 means first frame
--    - 'Frame_select' = 0 means second frame

  process(clk_80MHz)
  begin
    if (clk_80MHz'event and clk_80MHz = '1') then
      if rst = '1' then
        strobe       <= (others => '1');
        dataout      <= (others => '1');
        Frame_select <= '0';
      else
        -- Shift register
        for i in lenght_strobe-1 downto 1 loop
          strobe(i)  <= strobe(i-1);
          dataout(i) <= dataout(i-1);
        end loop;
        strobe(0)  <= cati_strobe;
        dataout(0) <= cati_dataout;
        -- Falling edge detector toggles "frame select"
        -- Anti glitch detection with a long shift register
        if (strobe = X"F"&X"0") then
          Frame_select <= not Frame_select;
        else
          Frame_select <= Frame_select;
        end if;
      end if;
    end if;
  end process;

  -- ** New event detection

-- Detects a new event when there is a new falling edge, and "frame
-- select" is low, so a new event begins at start of a 29-periods long
-- frame.

-- Useful for computing the s-curve using the data (event) method; in this
-- case, this signal replaces the input trigger (discri) signal, useful when
--                                                                    in trigger method

  process(clk_80MHz)
  begin
    if (clk_80MHz'event and clk_80MHz = '1') then
      if rst = '1' then
        NewEvent <= '0';
      else
        if (strobe = X"F"&X"0") and (Frame_select = '0') then
          NewEvent <= '1';
        else
          NewEvent <= '0';
        end if;
      end if;
    end if;
  end process;

  -- ** Data to fifo process

--    Store input data in two seperate fifos following status of the
--       'Frame_select' flag
--       Frame 1 goes to fifo1, frame 2 goes to fifo2

--       Inputs:

--       - dataout

-- Outputs:

--  - Fifo_data_in1 (32)
--  - Fifo_wreq_frame1 (32)

--  - Fifo_data_in2 (32)
--       - Fifo_wreq_frame2 (32)

  process (clk_80MHz)
  begin

    if (clk_80MHz'event and clk_80MHz = '1') then

      if rst = '1' then

        Fifo_wreq_frame1 <= '0';
        Fifo_wreq_frame2 <= '0';
        Fifo_data_in1    <= X"ABCDABCD";
        Fifo_data_in2    <= X"ABCDABCE";
        Frame_bitContor  <= (others => '0');

      else

        -- frame 1
        if (strobe(lenght_strobe-1) = '0' and Frame_select = '1') then

          Fifo_wreq_frame2 <= '0';
          Fifo_data_in1    <= "00" & output_nb & Fifo_data_in1(27 downto 0) & dataout(lenght_strobe-1);
          if (Frame_bitContor = 28) then  --28
            Frame_bitContor  <= (others => '0');
            Fifo_wreq_frame1 <= '1';
          else
            Frame_bitContor  <= Frame_bitContor + 1;
            Fifo_wreq_frame1 <= '0';
          end if;

        -- frame 2
        elsif (strobe(lenght_strobe-1) = '0' and Frame_select = '0') then

          Fifo_wreq_frame1 <= '0';
          if (nbBitselect = '1') then

            -- 21 bits frame
            Fifo_data_in2 <= X"00" & "000" & Fifo_data_in2(19 downto 0) & dataout(lenght_strobe-1);
            if (Frame_bitContor = 20) then  --20
              Frame_bitContor  <= (others => '0');
              Fifo_wreq_frame2 <= '1';
            else
              Frame_bitContor  <= Frame_bitContor + 1;
              Fifo_wreq_frame2 <= '0';
            end if;

          else

            -- 11 bits frame
            Fifo_data_in2 <= X"00000" & '0' & Fifo_data_in2(9 downto 0) & dataout(lenght_strobe-1);
            if (Frame_bitContor = 10) then  --20
              Frame_bitContor  <= (others => '0');
              Fifo_wreq_frame2 <= '1';
            else
              Frame_bitContor  <= Frame_bitContor + 1;
              Fifo_wreq_frame2 <= '0';
            end if;
          end if;

        else

          Fifo_wreq_frame1 <= '0';
          Fifo_wreq_frame2 <= '0';
          Fifo_data_in1    <= (others => '1');
          Fifo_data_in2    <= (others => '1');
          Frame_bitContor  <= (others => '0');

        end if;

        -- if (strobe(lenght_strobe-1) = '1' and Frame_select = '0') then
        --    -- if Fifo_full_frame2 = '0' then
        --    --    enable <= '1';
        --    -- else
        --    --    enable <= '0';
        --    -- end if;
        --    enable <= not(Fifo_full_frame2);
        -- end if;

      end if;

    end if;

  end process;

  -- ** Gray decode frame 1

  -- Convert from gray to unsigned binary the frame 1 data.

  --    Inputs:

  --    - Fifo_wreq_frame1
  --    - Fifo_data_in1

  --    Outputs:

  --    - Fifo_data_in1_f1
  --    - Fifo_wreq_frame1_f1

  process (clk_80MHz)
    variable Fifo_wreq_frame1_f1_v : std_logic                     := '0';
    variable Fifo_data_in1_f1_v    : std_logic_vector(31 downto 0) := (others => '0');
  begin
    if (clk_80MHz'event and clk_80MHz = '1') then
      if rst = '1' then
        Fifo_wreq_frame1_f1 <= '0';
        Fifo_data_in1_f1    <= (others => '1');
      else
        Fifo_wreq_frame1_f1_v            := Fifo_wreq_frame1;
        Fifo_data_in1_f1_v(31 downto 29) := Fifo_data_in1(31 downto 29);
        -- Nb. Channel
        Fifo_data_in1_f1_v(28)           := Fifo_data_in1(28);
        for i in 27 downto 26 loop
          Fifo_data_in1_f1_v(i) := Fifo_data_in1(i) xor Fifo_data_in1_f1_v(i+1);
        end loop;
        -- Coarse Time
        Fifo_data_in1_f1_v(25) := Fifo_data_in1(25);
        for i in 24 downto 0 loop
          Fifo_data_in1_f1_v(i) := Fifo_data_in1(i) xor Fifo_data_in1_f1_v(i+1);
        end loop;
        -- Take decoded value or not
        if decode_gray = '1' then
          Fifo_wreq_frame1_f1 <= Fifo_wreq_frame1_f1_v;
          Fifo_data_in1_f1    <= Fifo_data_in1_f1_v;
        else
          Fifo_wreq_frame1_f1 <= Fifo_wreq_frame1;
          Fifo_data_in1_f1    <= Fifo_data_in1;
        end if;
      end if;
    end if;
  end process;

  -- *** Event counter management

--    Where I manage an event counter by channel (only in frame 1).

-- I have previously [[*Event counter signals][declared]] an =EventCounter= array. Following the status
-- of the =Fifo_wreq_frame1_f1= flag, I increase the value of the counter
-- corresponding to current event channel by +1. To index the counter to be
-- increased, I use the four bits =ChannelNb= which contains the channel
-- number.

-- I append to the 32 bits from frame 1 of catiroc in =Fifo_data_in1_f1=,
-- 12 more bits with the correct event count, plus 4 zeros to get to 48
-- bits (size of the data bus of the fifo to store the data).

-- I need to proceed this way due to the fact that the channel number
-- appears in frame 1, whereas in the 32 bits of frame 2 there are 11
-- free bits to store the event counter information. In a next stage I
-- will [[*Write state][rearrange]] things to stay in 64-bit words, but here I
-- need to extend temporally from 32 bits to 48 bits. Same for the other
-- frame.

-- Inputs:

--  - Fifo_data_in1_f1
--  - Fifo_wreq_frame1_f1

-- Outputs:

--  - Fifo_data_in1_f1_del
--       - Fifo_wreq_frame1_f1_del

  ChannelNb <= unsigned(Fifo_data_in1_f1(29 downto 26));

  process (clk_80MHz)
  begin
    if (clk_80MHz'event and clk_80MHz = '1') then
      if rst = '1' then
        Fifo_data_in1_f1_del    <= X"ABCDABCDABCD";
        Fifo_wreq_frame1_f1_del <= '0';
      else
        Fifo_data_in1_f1_del <=
          X"0" & '0' &
          std_logic_vector(EventCounters(to_integer(ChannelNb))) &
          Fifo_data_in1_f1;
        Fifo_wreq_frame1_f1_del <= Fifo_wreq_frame1_f1;
      end if;
      if rst = '1' then
        EventCounters     <= (others => (others => '0'));
        NewEventByChannel <= (others => '0');
      elsif Fifo_wreq_frame1_f1 = '1' then
        EventCounters(to_integer(ChannelNb)) <=
          EventCounters(to_integer(ChannelNb)) + 1;
        NewEventByChannel(to_integer(ChannelNb)) <= '1';
      else
        EventCounters(to_integer(ChannelNb)) <=
          EventCounters(to_integer(ChannelNb));
        NewEventByChannel <= (others => '0');
      end if;
    end if;
  end process;

  -- ** Gray decode frame 2

--    Convert from gray to unsigned binary the frame 1 data.

-- Inputs:

--  - Fifo_wreq_frame2 (32)
--  - Fifo_data_in2 (32)

-- Outputs:

--  - Fifo_data_in2_f2 (48)
--  - Fifo_wreq_frame2_f2 (48)

-- Note that I need to append 16 bits with zeros to the 32 bits frame
-- word in order to write into the 48 bits data bus fifo. This data is
--       useless.

  process (clk_80MHz)
    variable Fifo_wreq_frame2_f2_v : std_logic                     := '0';
    variable Fifo_data_in2_f2_v    : std_logic_vector(31 downto 0) := (others => '0');
  begin
    if (clk_80MHz'event and clk_80MHz = '1') then
      if rst = '1' then
        Fifo_wreq_frame2_f2 <= '0';
        Fifo_data_in2_f2    <= (others => '1');
      else
        Fifo_wreq_frame2_f2_v            := Fifo_wreq_frame2;
        Fifo_data_in2_f2_v(31 downto 20) := Fifo_data_in2(31 downto 20);
        -- Time
        Fifo_data_in2_f2_v(19)           := Fifo_data_in2(19);
        for i in 18 downto 10 loop
          Fifo_data_in2_f2_v(i) := Fifo_data_in2(i) xor Fifo_data_in2_f2_v(i+1);
        end loop;
        -- ADC
        Fifo_data_in2_f2_v(9) := Fifo_data_in2(9);
        for i in 8 downto 0 loop
          Fifo_data_in2_f2_v(i) := Fifo_data_in2(i) xor Fifo_data_in2_f2_v(i+1);
        end loop;
        -- Take decoded value or not
        if decode_gray = '1' then
          Fifo_wreq_frame2_f2 <= Fifo_wreq_frame2_f2_v;
          Fifo_data_in2_f2    <= X"0000" & Fifo_data_in2_f2_v;
        else
          Fifo_wreq_frame2_f2 <= Fifo_wreq_frame2;
          Fifo_data_in2_f2    <= X"0000" & Fifo_data_in2;
        end if;
      end if;
    end if;
  end process;

  -- ** Fifo: frame 1

  --   Stores data coming from frame 1

  --      Inputs:

  -- - Fifo_data_in1_f1_del (48)
  --      - Fifo_wreq_frame1_f1_del

  Cati_frame1_fifo : entity work.Cati_frame_fifo
    port map (clk         => clk_80MHz,
              rst         => rst,
              din         => Fifo_data_in1_f1_del,
              wr_en       => Fifo_wreq_frame1_f1_del,
              rd_en       => Fifo_rden_frames,
              dout        => q1,
              full        => open,
              almost_full => open,
              empty       => open);

  -- ** Fifo: frame 2

--    Stores data coming from frame 2

-- Inputs:

--  - Fifo_data_in2_f2
--       - Fifo_wreq_frame2_f2

  Cati_frame2_fifo : entity work.Cati_frame_fifo
    port map (clk         => clk_80MHz,
              rst         => rst,
              din         => Fifo_data_in2_f2,
              wr_en       => Fifo_wreq_frame2_f2,
              rd_en       => Fifo_rden_frames,
              dout        => q2,
              full        => open,
              almost_full => open,
              empty       => Fifo_empty_frame2);

  -- ** Merge data process

  -- Here I transfer data from two previous, partial 48 bits fifos, to a
  --    full 64 bit data fifo.

  process(clk_80MHz)
  begin

    if (clk_80MHz'event and clk_80MHz = '1') then

      if rst = '1' then

        -- do nothing
        Fifo_wreq                  <= '0';
        data_in                    <= (others => '0');
        Fifo_rden_frames           <= '0';
        dataout_uniFramefifo_state <= standby;

      else

        case dataout_uniFramefifo_state is

          -- *** Standby state

--                Wait for data in frame 2 fifo. As data in frame 1 arrives before data
-- in frame 2, and number of words is the same, I only inspect the empty
-- flag in fifo 2: if there is something here, there will be the same
-- amount of 32 bit words in fifo 1.

          when standby =>

            Fifo_wreq <= '0';
            data_in   <= (others => '0');
            if (Fifo_full = '0' and Fifo_empty_frame2 = '0') then
              Fifo_rden_frames           <= '1';
              dataout_uniFramefifo_state <= write_tempo;
            else
              Fifo_rden_frames           <= '0';
              dataout_uniFramefifo_state <= standby;
            end if;

            -- *** Write state

--                   Send data read in fifos 1 and 2 to a 64 bits, full event fifo.

-- Notice that we rearrange things here. The 32 bits lowest weight bits
-- of data from fifo 1 are taken, except bit 30, which gets replaced by
-- the gain bit =q2(20)=. Then we append 20 bits from fifo 2, without the
-- gain bit. The remaining 12 bits are taken from fifo 2 (zeros), with
-- the “extra” appended data to frame 1 in fifo 1, which contains 12
-- bits of event counter. This way, we include an event counter with each
--                                physical event.

          when write_tempo =>

            Fifo_wreq                  <= '1';
            data_in                    <= q1(31 downto 0) & q2(20) & q1(42 downto 32) & q2(19 downto 0);
            Fifo_rden_frames           <= '0';
            dataout_uniFramefifo_state <= standby;

        end case;
      end if;
    end if;
  end process;

-- ** Big FIFO

  -- All data ends here, 64 bits (one event containing the two frames)
  --    input data bus.

  Cati_unifFifo_inst1 : entity work.Cati_unifFifo
    port map (clk   => clk_80MHz,
              rst   => rst,
              din   => data_in,
              wr_en => Fifo_wreq,
              rd_en => fifo_rd,
              dout  => fifo_dataout,
              full  => fifo_full,
              empty => fifo_empty);

end architecture Behavioral;
