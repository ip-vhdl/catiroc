-- * catiroc_capture_1ch

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;
library UNISIM;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use UNISIM.VComponents.all;
use work.catiroc_pkg.all;
use work.ft2232h_instance_pkg.all;
use work.edacom_pkg.all;

-- * Entity

entity catiroc_capture_1ch is
  port(catiroc           : inout rc_catiroc;  -- cati
       global_reset      : in    std_logic;
       resetb            : out   std_logic;
       --
       NewEvent          : out std_logic;
       NewEventByChannel : out std_logic_vector(15 downto 0);
       clk_80Mhz         : in  std_logic;
       clk_80MHz_delayed : in  std_logic;
       clk_40Mhz         : in  std_logic;
       clk_480MHz        : in  std_logic;
       clk_120MHz        : in  std_logic;
       clk_160MHz        : in  std_logic;
       pll_locked        : in  std_logic;
       discri_falling    : out std_logic_vector(15 downto 0);
       params            : in  std_logic_vector (7 downto 0);
       enable            : in  std_logic;
       --
       fifo_rd_if   : inout t_fifo_rd_if
       -- fifo_if_clk   : in  std_logic;
       -- fifo_if_rd    : in  std_logic;
       -- fifo_if_empty : out std_logic;
       -- fifo_if_data  : out std_logic_vector(p2_data_width*8-1 downto 0)
       );
end entity catiroc_capture_1ch;

-- * Architecture

architecture Behavioral of catiroc_capture_1ch is

  type TriggerCounters is array(0 to 31) of unsigned(15 downto 0);

  -- ** Signals

   signal StartSys : std_logic := '0';
   -- signal gnd_1bit : std_logic := '0';
   -- signal vcc_1bit : std_logic := '0';

   type dataout_fifo_state_typee is (standby, write_tempo, write_tempo2);
-- component generics
   constant nb_channels                          : natural range 1 to 8                                := 3;
   constant active_channels                      : std_logic_vector(7 downto 0)                        := "00000001";
   constant Seuil_fifos_empty                    : integer                                             := 100;
-- component ports
   signal dataout, strobe                        : std_logic                                           := '0';
   signal dataout2, strobe2                      : std_logic                                           := '0';
   -- signal init_standby_period                    : unsigned(7 downto 0)                                := (others => '0');
-- internals signals
   signal main_fifo_datain, main_fifo_datain_tmp : std_logic_vector(63 downto 0)                       := (others => '0');
   signal main_fifo_wr                           : std_logic                                           := '0';
   signal main_fifo_full, rst_counter            : std_logic                                           := '0';
   signal fifo_output1_empty, resetb_inner       : std_logic                                           := '0';
   signal fifo_output1_rd                        : std_logic                                           := '0';
   signal fifo_output1_data                      : std_logic_vector(63 downto 0)                       := (others => '0');
   signal fifo_output2_empty                     : std_logic                                           := '0';
   signal fifo_output2_rd                        : std_logic                                           := '0';
   signal fifo_output2_data                      : std_logic_vector(63 downto 0)                       := (others => '0');
   signal fifo_output3_empty                     : std_logic                                           := '0';
   signal fifo_output3_rd                        : std_logic                                           := '0';
   signal rst_discri_data, rst_physical_data     : std_logic                                           := '1';
   signal rst_special_data                       : std_logic                                           := '1';
   signal fifo_output3_data                      : std_logic_vector(63 downto 0)                       := (others => '0');
   signal cati_standby_period                    : unsigned(7 downto 0)                                := (others => '0');
   signal counter                                : unsigned(24 downto 0)                               := (others => '0');
   signal ChannelNb                              : unsigned(4 downto 0)                                := (others => '0');
   -- signal reset_cati                             : std_logic                                           := '0';
   signal dataout_fifo_state                     : dataout_fifo_state_typee;
   signal rst, NewEvent1, NewEvent2              : std_logic                                           := '0';
   signal priority_flag                          : unsigned(1 downto 0)                                := "00";
   -- signal cati_reset_delayed                     : std_logic_vector(31 downto 0)                       := (others => '0');
   signal tdc_data_f, tdc_data_r                 : std_logic_vector(6*c_nb_discri_channels-1 downto 0) := (others => '0');
   signal tstamp                                 : std_logic_vector(31 downto 0)                       := (others => '0');
   signal tstamp_u                               : unsigned(31 downto 0)                               := (others => '0');
   signal clk_40MHz_out                          : std_logic                                           := '0';
   signal clk_160MHz_out                         : std_logic                                           := '0';

  -- Enables reduction of discriminator data.
  alias ReduceDiscriminatorData : std_logic is params(5);

  -- Enables the special data stream feature when set high.
  alias EnableSpecialData : std_logic is params(6);

  -- Enables the physical data stream feature when set high.
  alias EnablePhysicalData : std_logic is params(4);

  -- Enables the discriminator data stream feature when set high.
  alias EnableDiscriminatorData : std_logic is params(3);

  alias DecodeGray     : std_logic is params(2);
  alias nbBitselect    : std_logic is params(1);
  alias EmulateCatiROC : std_logic is params(0);

  -- This signal will store events, by channel.
  signal NewEventByChannel_0_to_7 : std_logic_vector(15 downto 0) := (others => '0');
  signal NewEventByChannel_8_to_15 : std_logic_vector(15 downto 0) := (others
                                                                       => '0');

  signal cati_dataout, cati_dataout_sampled, cati_dataout_in : std_logic_vector(1 downto 0) :=
    (others => '0');

  signal cati_strobe_sampled, cati_strobe_in : std_logic_vector(1 downto 0) :=
    (others => '0');

  -- ** Data rate measurement signals

-- Used for [[*Source 4: Data Rate][data rate]] measurements. By channel:

--  - TriggerCounters_10ms :: gets updated every 10 ms, taking the value of
--       TriggerCounters_up; will be readout with each event

--                              - TriggerCounters_up   :: +1 every new trigger; gets reset every 10 ms

  signal TriggerCounters_10ms                                     : TriggerCounters               := (others => X"0000");
  signal TriggerCounters_up                                       : TriggerCounters               := (others => X"0000");
  signal triggers_latched, triggers_latched_2, triggers_latched_3 : std_logic_vector(15 downto 0) := (others => '0');
  signal triggers_enabled_tmp                                     : std_logic_vector(15 downto 0) := (others => '0');
  signal triggers_enabled_tmp_del                                 : std_logic_vector(15 downto 0) := (others => '0');
  signal triggers_top_rising, triggers_top_falling                : std_logic_vector(15 downto 0) := (others => '0');
  signal Counter10msTop                                           : std_logic                     := '0';
  signal Counter10ms                                              : unsigned(19 downto 0)         := (others => '0');

begin

  -- ** Startsys and val_evt

  -- StartSys must be synchronous to 40MHz clock domain
  process (clk_40MHz)
  begin
    if rising_edge(clk_40MHz) then
      catiroc.StartSys <= StartSys;
    end if;
  end process;

  U_OBUFDS : OBUFDS
    port map (I  => StartSys,
              O  => catiroc.val_evt.p,
              OB => catiroc.val_evt.n);

  catiroc.pwr_on <= '1';

  -- ** Clocks 160MHz and 40MHz

  -- *** Replicate 160 MHz clock out of the fpga

  U_oddr_2 : ODDR
    generic map(DDR_CLK_EDGE => "OPPOSITE_EDGE",
                INIT         => '1',
                SRTYPE       => "SYNC")
    port map (Q  => clk_160MHz_out,
              C  => clk_160MHz,
              CE => '1',
              D1 => '1',
              D2 => '0',
              R  => '0',
              S  => '0');

  U_OBUFDS_3 : OBUFDS
    port map (I  => clk_160MHz_out,
              O  => catiroc.clk_160MHz.p,
              OB => catiroc.clk_160MHz.n);

  -- *** Replicate 40 MHz clock out of the fpga

  U_oddr_1 : ODDR
    generic map(DDR_CLK_EDGE => "OPPOSITE_EDGE",
                INIT         => '0',
                SRTYPE       => "SYNC")
    port map (Q  => clk_40MHz_out,
              C  => clk_40MHz,
              CE => '1',
              D1 => '1',
              D2 => '0',
              -- R  => disable_40MHz,
              R  => '0',
              S  => '0');

  U_OBUFDS_2 : OBUFDS
    port map (I  => clk_40MHz_out,
              O  => catiroc.clk_40MHz.p,
              OB => catiroc.clk_40MHz.n);

  -- ** Reset

  -- Common reset to all blocks and send active low reset to ASIC.

  --    Generates a reset period right after 'p2_enable' goes high; sends
  --                                                                   this reset to ASIC, so that first 29-periods length frame will come
  --                                                                   afterwards; I delay here to be sure catiroc starts sending data after
  --                                                                                  the logic is ready.

  u_rest_cati : process (clk_80Mhz)
  begin
    if rising_edge(clk_80Mhz) then
      if global_reset = '1' or enable = '0' then  --or reset_cati = '1' then
        rst                 <= '1';
        resetb_inner        <= '0';
        StartSys            <= '0';
        cati_standby_period <= (others => '0');
      else
        if cati_standby_period <= X"40" then
          cati_standby_period <= cati_standby_period + 1;
          rst                 <= '1';
          StartSys            <= '0';
          resetb_inner        <= '0';
        elsif cati_standby_period <= X"A0" then
          cati_standby_period <= cati_standby_period + 1;
          rst                 <= '0';
          StartSys            <= '0';
          resetb_inner        <= '0';
        elsif cati_standby_period < X"FF" then
          cati_standby_period <= cati_standby_period + 1;
          rst                 <= '0';
          StartSys            <= '0';
          resetb_inner        <= '1';
        else
          rst          <= '0';
          StartSys     <= '1';
          resetb_inner <= '1';
        end if;
      end if;
    end if;
  end process u_rest_cati;

  resetb <= resetb_inner;

  -- ** Capture data from ASIC

  -- *** Front end data sampling

  -- Differential to single ended
  -- Convert from LVDS differential to single ended. Xilinx ISE 14.7 and Quartus 13
  -- handle this differently.
  u_cati_dataout_1 : for j in 0 to 1 generate
    u_ibufds : ibufds
      port map (O  => cati_dataout(j),
                I  => catiroc.cati_dataout(j).p,
                IB => catiroc.cati_dataout(j).n);
  end generate;

  -- Sample incoming data
  -- Common to the two boards: sample data with a delayed 80 MHz clock (delay is
  -- manually tuned), then get back to the 80 MHz clock domain. This is necessary as
  -- CatiROC doesn’t provide a clock along with the data.
  U_dataout_sample : process(clk_80MHz_delayed)
  begin
    if rising_edge(clk_80MHz_delayed) then
      cati_dataout_sampled <= cati_dataout;
      cati_strobe_sampled  <= catiroc.cati_strobe;
    end if;
  end process;

  -- Double sample incoming data
  -- Careful must be paid here: following the delay, timing may be difficult to meet
  --    between these two stages.
  U_dataout_in : process(clk_80MHz)
  begin
    if rising_edge(clk_80Mhz) then
      cati_dataout_in <= cati_dataout_sampled;
      cati_strobe_in  <= cati_strobe_sampled;
    end if;
  end process;

  -- *** Source 1: Channels 0 to 7

  rst_physical_data <= '1' when (rst = '1' or EnablePhysicalData = '0') else '0';

  -- **** Pattern generator

-- Pattern generator: only for test purposes.

  cati_dataout_pattern_1 : entity work.catiroc_emulator
    generic map (nb_channels     => 1,
                 active_channels => active_channels)
    port map (clk_in       => clk_80Mhz,
              rst          => rst_physical_data,
              emulated     => EmulateCatiROC,
              cati_dataout => cati_dataout_in(0),
              cati_strobe  => cati_strobe_in(0),
              clk_out      => open,
              dataout      => dataout,
              strobe       => strobe);

  -- **** Data capture

  u_catiroc_capture_1ch_core_1 : entity work.catiroc_capture_1ch_core
    generic map (output_nb     => '0',
                 lenght_strobe => 8)
    port map (rst               => rst_physical_data,
              NewEvent          => NewEvent1,
              NewEventByChannel => NewEventByChannel_0_to_7,
              --cati
              clk_80Mhz         => clk_80Mhz,
              cati_dataout      => dataout,
              cati_strobe       => strobe,
              nbBitselect       => nbBitselect,
              decode_gray       => DecodeGray,
              --peripheral
              fifo_dataout      => fifo_output1_data,
              fifo_rd           => fifo_output1_rd,
              fifo_empty        => fifo_output1_empty);

  -- *** Source 2: Channels 8 to 15

  -- **** Pattern generator

  -- Pattern generator: only for test purposes.

  cati_dataout_pattern_2 : entity work.catiroc_emulator
    generic map (nb_channels     => 1,
                 active_channels => active_channels)
    port map (clk_in       => clk_80Mhz,
              rst          => rst_physical_data,
              emulated     => EmulateCatiROC,
              cati_dataout => cati_dataout_in(1),
              cati_strobe  => cati_strobe_in(1),
              clk_out      => open,
              dataout      => dataout2,
              strobe       => strobe2);

  -- **** Data capture

  u_handle_cati_data_out_2 : entity work.catiroc_capture_1ch_core
    generic map (output_nb     => '1',
                 lenght_strobe => 8)
    port map (rst               => rst_physical_data,
              NewEvent          => NewEvent2,
              NewEventByChannel => NewEventByChannel_8_to_15,
              --cati
              clk_80MHz         => clk_80Mhz,
              cati_dataout      => dataout2,
              cati_strobe       => strobe2,
              nbBitselect       => nbBitselect,
              decode_gray       => DecodeGray,
              --peripheral
              fifo_dataout      => fifo_output2_data,
              fifo_rd           => fifo_output2_rd,
              fifo_empty        => fifo_output2_empty);

  -- **** New event oring

  -- One event in one of the 16 channels

  process (clk_80MHz) is
  begin
    if clk_80MHz'event and clk_80MHz = '1' then
      NewEvent <= NewEvent1 or NewEvent2;
    end if;
  end process;

  -- *** Source 3: Discriminator data

  -- Here, we are going to generate another data stream starting from the triggers
  -- outputs of catiroc. We are going to detect its edges (falling and rising), and
  -- time tag them along with an event number. In the case of the omega test card,
  -- only a time stamp will be used; in the case of the juno board, a fine time will
  -- be computed using a ~1 ns. accuracy time to digital converter (TDC).

  -- **** Trigger events and TDC

   U0 : for i in 0 to 15 generate
     process (catiroc.triggers(i), triggers_enabled_tmp(i))
     begin
         if (triggers_enabled_tmp(i) = '1' and catiroc.triggers(i) = '1') then
            triggers_latched(i) <= '0';
         elsif catiroc.triggers(i)'event and catiroc.triggers(i) = '0' then
            triggers_latched(i) <= '1';
         end if;
      end process;

      process (clk_80MHz)
      begin
         if clk_80MHz'event and clk_80MHz = '1' then
            triggers_latched_2(i)   <= triggers_latched(i);
            triggers_enabled_tmp(i) <= triggers_latched_2(i);
         end if;
      end process;

            process (clk_80MHz)
      begin
         if clk_80MHz'event and clk_80MHz = '1' then
            triggers_enabled_tmp_del(i) <= triggers_enabled_tmp(i);
            triggers_top_falling(i)     <= triggers_enabled_tmp(i) and (triggers_enabled_tmp_del(i) xor triggers_enabled_tmp(i));
            triggers_top_rising(i)      <= triggers_top_falling(i);
         end if;
      end process;
   end generate U0;

   discri_falling <= triggers_top_falling;
-- @@@ Omega test board code:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Omega%20test%20board%20code][Omega test board code:3]]
  -- **** Omega test board code

   process(clk_80MHz)
   begin
      if (clk_80MHz'event and clk_80MHz = '1') then
         if rst = '1' or catiroc.ovf = '1' or resetb_inner = '0' then
            tstamp_u <= (others => '0');
         else
            tstamp_u <= tstamp_u + 1;
         end if;
      end if;
   end process;

   tstamp <= std_logic_vector(tstamp_u);
-- @@@ Omega test board code:3 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Omega%20test%20board%20code][Omega test board code:4]]
  -- **** Omega test board code

   tdc_data_f <= (others => '0');
   tdc_data_r <= (others => '0');

  -- First, we need to detect edges in incoming local triggers from the ASIC, and
  -- produce a time stamp to label its arrival time.

  -- In this case, we embed a real =tdc= within each input pin. This will provide a
  -- fine time measurement (=tdc_data_f= and =tdc_data_r=) in addition to the coarse time
  -- value in =tstamp=, which will use 26 bits @ 40 MHz for compatibility with that of
  -- the ASIC.

  -- U0_tdc : entity work.tdc
  --    generic map (TDC_N => c_nb_discri_channels)
  --    port map (data_in_from_pins => catiroc.triggers,
  --              ovf               => catiroc.ovf,
  --              resetb            => resetb_inner,
  --              clk_sync          => clk_40MHz,
  --              clk_s             => clk_120MHz,
  --              clk_f             => clk_480MHz,
  --              lock              => pll_locked,
  --              tstamp            => tstamp,
  --              main_rst          => global_reset,
  --              data_out_f        => tdc_data_f,
  --              data_out_r        => tdc_data_r,
  --              falling_out       => triggers_top_falling,
  --              rising_out        => triggers_top_rising);

  -- discri_falling <= triggers_top_falling;

  -- **** Data capture

  -- Here, we capture the previous into a stream.

  rst_discri_data <= '1' when (rst = '1' or EnableDiscriminatorData = '0') else '0';

  u_catiroc_discriminator_data : entity work.catiroc_discriminator_data
    port map (rst                     => rst_discri_data,
              ovf                     => catiroc.ovf,
              resetb                  => resetb_inner,
              clk_80MHz               => clk_80Mhz,
              clk_40MHz               => clk_40Mhz,
              discri_rising           => triggers_top_rising,
              discri_falling          => triggers_top_falling,
              discri_data_f           => tdc_data_f,
              discri_data_r           => tdc_data_r,
              tstamp                  => tstamp,
              ReduceDiscriminatorData => ReduceDiscriminatorData,
              -- fifo if
              fifo_dataout            => fifo_output3_data,
              fifo_rd                 => fifo_output3_rd,
               fifo_empty              => fifo_output3_empty);

  -- ** Source 4: Data Rate


  -- **** Data Rate: justification

  -- The idea here is to readout, by channel and appending this information to each
  -- new event from catiroc, a 16 bits word containing the number of triggers withing
  -- a 10 ms window. Why 10 ms ?

  --  - < 100 events/s :: we get one or no count within a 10 ms window
  --  - 100 events/s   :: this means one event every 10 ms, within a 10 ms window we
  --       will count one event
  --  - 1000 events/s  :: this means one event every 1 ms, within a 10 ms window we
  --       will count 10 events (on average)
  --  - 10k events/s   :: this means one event every 100 us, within a 10 ms window we
  --       will count 100 events (on average)
  --  - 100k events/s   :: this means one event every 10 us, within a 10 ms window we
  --      will count 1k events (on average)
  --  - 200k events/s   :: this means one event every 5 us, within a 10 ms window we
  --      will count 2k events (on average)
  --  - 1M events/s   :: this means one event every 1 us, within a 10 ms window we
  --      will count 10k events (on average)
  --  - 5M events/s   :: this means one event every 200 ns, within a 10 ms window we
  --      will count 50k events (on average)

  -- As we have 16 bits to readout this information, a 10 ms time window makes we are
  -- limited to a bit more than 5M events/s, with a lower limit of 100 events/s. This
  -- is more than adequate for this application.

  -- Then, by software, dividing this information by 10 ms will provide the event
  -- rate as seen by the CatiROC discriminator.

  -- *** Data Rate: 10 ms counter

  U_10ms : process (clk_80MHz) is
  begin
    if clk_80MHz'event and clk_80MHz = '1' then
      if global_reset = '1' then
        Counter10msTop <= '0';
        Counter10ms    <= (others => '0');
      else
        if Counter10ms < 800000 then
          Counter10msTop <= '0';
          Counter10ms    <= Counter10ms + 1;
        else
          Counter10msTop <= '1';
          Counter10ms    <= (others => '0');
        end if;
      end if;
    end if;
  end process U_10ms;

  -- *** Data Rate: counting

  -- In this case, I have to create a =triggers_top_rising= and one
  -- =triggers_top_falling= one clock pulse by detected trigger edge.

  -- First, I capture a falling edge async external event into a sync clock domain.
  -- The output might have more than one clock cycle length.

  --    - _Discriminator events data stream_

  -- Now we proceed by channel. First, we use the =triggers_top_rising= one clock pulse
  -- by detected trigger, +1 increasing =TriggerCounters_up=. The later, will get reset
  -- every 10 ms; at this very moment, =TriggerCounters_10ms= takes its value. The
  -- later will store the previous total sum during the next 10 ms, and will be
  -- appended to the first 16 words in the special event readout stream.

  U_data_rate_discri : for i in 0 to 15 generate
    U_data_rate_discri_channel : process (clk_80MHz) is
    begin
      if clk_80MHz'event and clk_80MHz = '1' then
        if global_reset = '1' then
          TriggerCounters_up(i)   <= (others => '0');
          TriggerCounters_10ms(i) <= (others => '0');
        else
          if Counter10msTop = '1' then
            TriggerCounters_up(i)   <= X"0000";
            TriggerCounters_10ms(i) <= TriggerCounters_up(i);
          elsif triggers_top_rising(i) = '1' then
            TriggerCounters_up(i) <= TriggerCounters_up(i) + 1;
          end if;
        end if;
      end if;
    end process U_data_rate_discri_channel;
  end generate U_data_rate_discri;

  -- *** Data Rate: counting


  -- - _Physical events data stream_

  -- I just set the remaining special events stream to zero.

  U_data_rate_physical : for i in 16 to 31 generate
    TriggerCounters_up(i)   <= (others => '0');
    TriggerCounters_10ms(i) <= (others => '0');
  end generate U_data_rate_physical;

  -- *** Data Rate: disable feature

  rst_special_data <= '1' when (rst = '1' or EnableSpecialData = '0') else '0';

  -- ** New event by channel

  -- Concatenation of events, by channel.

  NewEventByChannel <= NewEventByChannel_8_to_15(15 downto 8) &
                       NewEventByChannel_0_to_7(7 downto 0);


  -- ** Replicate ASIC Coarse time                                       :xilinx:

-- Mainly useful in the context of Juno.

-- The idea here is to replicate CatiROC coarse time.

--  - =resetb= :: (active low) resets all digital logic in the asic, so the coarse
--                time too
--  - =ovf= :: (active high) tells the 26 bits counter overflows, useful here
--             (redundant) ??

-- Don’t use here

--  - =rst= :: active when this peripheral is enabled, once at the beginning

-- as this would make the two TS shift (=ovf= and =rst= are delayed).

-- #+begin_src vhdl-tools :tangle no
--   process(clk_40MHz)
--   begin
--      if (clk_40MHz'event and clk_40MHz = '1') then
--         if ovf = '1' or resetb_inner = '0' then
--            CoarseTime <= (others => '0');
--         else
--            CoarseTime <= CoarseTime + 1;
--         end if;
--      end if;
--   end process;
-- #+end_src

  -- ** Coincidence data taking

-- Mainly useful in the context of Juno, as coincidence data taking is equivalent
-- to zero suppression or to oscilloscope like data taking.

-- The idea here is to take data in coincidence with an external trigger, which
-- opens a length L time window. Simply put, the fpga replicates the ASIC coarse
-- time, so one may assume both are identical.

-- Post trigger, L.

-- When a trigger arrives, the fpga takes a snapshot of the local coarse
-- time, T. From this point, it only takes events whose coarse time is comprised in
-- the interval [T:T+L]. It filters out the rest. This is easy to implement at the
-- end of the data flow with the current code. No matter when the events arrives to
-- the fpga, what counts is its coarse time, so events happenning before T (whose
-- coarse time is < T), and arriving after T will be treated correctly. All of this
-- is fine as long as L is < 2^26.

-- Pre trigger, L’.

-- Events whose CT is < T, but hit the fpga at a time > T are not a problem, as the
-- data flow sees them after the trigger arrival. Problem is events with a CT < T
-- within [T-L’:T], so that they should be taken, but arriving to the fpga
-- before T. The firmware is unable to take them as long as the trigger is not yet
-- there. No way to solve the issue without a bit of hacking.

-- The solution is to delay all events, adding artificially a latency, so that all
-- events, regardless of their CT, hit the fpga after T (arriving late is not a
-- problem, as long as their CT is there to filter them). In this case, a shift
-- register must be added to the very end of the events path. Implementation.

-- Currently, one big fifo stores all events, in the 80 MHz clock domain. This fifo
-- is read by the usb if in the 60 MHz clock domain. This must be modified so that
-- this big fifo is read, when events are available, at 80 MHz, and its contents
-- sent to a SR. The width of the SR is that of an event, plus a bit to store the
-- empty status of the big fifo: when no data is available, a zero event is push
-- into the SR, with the extra bit set to 1. This allows removing the empty event
-- at the SR output. The SR must have 100 positions, for example, for a L’ = 12.5
-- ns x 100, so that events with a CT = T - L’ are well delayed until T. When they
-- are readout of the SR, T is already there, and a decision may be taken. This
-- solved the problem with the pre trigger.

-- Note. Carefull with CT overflows is the case max(CT) < T+L. This is similar to
-- the way I used to handle oscilloscope mode, with post and pre trigger time
--                                                                        windows with TNT.

  -- ** Merge data from 4 sources


-- Here, I transfer physical events data words from previous fifos,
-- corresponding to first and second half of catiroc, to the next
-- level. I use =priority_flag= to give a chance to the two fifos, avoiding
-- one of them from saturating the transfer. In next fifo, we will get
-- full 64 bits events corresponding to all catiroc channels.

-- Additionally, I include special data words with specific
-- information. Currently, I readout trigger rate information by channel.

  process(clk_80Mhz)

  begin

    -- *** Reset state

    if (clk_80Mhz'event and clk_80Mhz = '1') then

      if rst = '1' then

        fifo_output1_rd    <= '0';
        fifo_output2_rd    <= '0';
        fifo_output3_rd    <= '0';
        main_fifo_datain   <= (others => '0');
        main_fifo_wr       <= '0';
        rst_counter        <= '0';
        dataout_fifo_state <= standby;
        priority_flag      <= "00";
        ChannelNb          <= (others => '0');

      else

        case dataout_fifo_state is

          -- *** Standby

          -- Waits for something to come from any of previous two fifos (not empty
          --                                                             priority_flag), while next fifo full priority_flag is not set.

          when standby =>

            ChannelNb        <= (others => '0');
            main_fifo_wr     <= '0';
            main_fifo_datain <= (others => '0');
            rst_counter      <= '0';
            if priority_flag = "10" then
              priority_flag <= "00";
            else
              priority_flag <= priority_flag + 1;
            end if;
            if (priority_flag = "00" and fifo_output1_empty = '0' and main_fifo_full = '0' and EnablePhysicalData = '1') then
              main_fifo_datain_tmp <= fifo_output1_data;
              fifo_output1_rd      <= '1';
              fifo_output2_rd      <= '0';
              fifo_output3_rd      <= '0';
              dataout_fifo_state   <= write_tempo;
            elsif (priority_flag = "01" and fifo_output2_empty = '0' and main_fifo_full = '0' and EnablePhysicalData = '1') then
              main_fifo_datain_tmp <= fifo_output2_data;
              fifo_output1_rd      <= '0';
              fifo_output2_rd      <= '1';
              fifo_output3_rd      <= '0';
              dataout_fifo_state   <= write_tempo;
            elsif (priority_flag = "10" and fifo_output3_empty = '0' and main_fifo_full = '0' and EnableDiscriminatorData = '1') then
              main_fifo_datain_tmp <= fifo_output3_data;
              fifo_output1_rd      <= '0';
              fifo_output2_rd      <= '0';
              fifo_output3_rd      <= '1';
              dataout_fifo_state   <= write_tempo;
            else
              main_fifo_datain_tmp <= X"00000000000000B0";
              fifo_output1_rd      <= '0';
              fifo_output2_rd      <= '0';
              fifo_output3_rd      <= '0';
              dataout_fifo_state   <= standby;
            end if;

            -- *** Sources 1-3: Physical event words + discriminator data

-- During the =write_tempo= state, data read in previous state is written to next
-- fifo. A 25 bits [[*Counter][counter]] measures the time since last time the special words were
-- readout. Following its value, we jump to standby state to proceed to next
-- physical event word or we proceed to sending special words containing specific
--    information.


          when write_tempo =>

            if priority_flag = "10" then
              priority_flag <= "00";
            else
              priority_flag <= priority_flag + 1;
            end if;
            fifo_output1_rd  <= '0';
            fifo_output2_rd  <= '0';
            fifo_output3_rd  <= '0';
            ChannelNb        <= (others => '0');
            --
            main_fifo_wr     <= '1';
            main_fifo_datain <= main_fifo_datain_tmp;
            rst_counter      <= '0';
            --
            if counter = '1' & X"FFFFFF" then
              dataout_fifo_state <= write_tempo2;
            else
              dataout_fifo_state <= standby;
            end if;

            -- *** Source 4 : Special word

-- Here, I transfer 16 special 64 bits words, one by channel, using the =ChannelNb=
-- counter. For each channel, I set in =main_fifo_datain= the two leftmost bits to “01”,
-- to mark this special word, and then I append the channel number and the trigger
-- rate information to be readout.

-- This way, I can readout the =TriggerCounters= registers independently of the event
-- information, which anyway, doesn’t contain any free space. Each of the 16
-- =TriggerCounters= will be readout sequentially with its corresponding channel
-- number. =TriggerCounters= remains stable during 10 ms.

-- Note that I need to be carefull about the status of the next fifo where I put
-- the data, so I need to monitor its full flag state.

-- At the end of all the 16 channels, I get back to the =standby= state to normal
--                                        operation, and I reset the counter.

          when write_tempo2 =>

            fifo_output1_rd <= '0';
            fifo_output2_rd <= '0';
            fifo_output3_rd <= '0';
            if main_fifo_full = '0' then
              main_fifo_wr     <= '1';
              ChannelNb        <= ChannelNb + 1;
              main_fifo_datain <= '0' &
                                  '1' &
                                  X"0" &
                                  "00" &
                                  X"000000" &
                                  "000" & X"00" &
                                  std_logic_vector(ChannelNb) &
                                  std_logic_vector(TriggerCounters_10ms(to_integer(ChannelNb)));
              if ChannelNb = '1' & X"F" then
                dataout_fifo_state <= standby;
                rst_counter        <= '1';
              else
                dataout_fifo_state <= write_tempo2;
                rst_counter        <= '0';
              end if;
            else
              main_fifo_wr <= '0';
            end if;

        end case;

      end if;

    end if;

  end process;

  -- ** Counter

-- This 25 bits (~100 ms. at 80 MHz.) counter measures the time since last time a
-- set of =special events= were readout. When it reaches its max it waits until a
-- =rst_counter= signal happens. This indicates that the =special events= have been
--    readout.

  process(clk_80Mhz)
  begin
    if (clk_80Mhz'event and clk_80Mhz = '1') then
      if (rst_counter = '1' or rst_special_data = '1') then
        counter <= (others => '0');
      else
        if counter < '1' & X"FFFFFF" then
          counter <= counter + 1;
        end if;
      end if;
    end if;
  end process;

  -- ** Big FIFO

-- All data ends here, 64 bits (one event containing the two frames)
-- input data bus.

--    This fifo implements the peripheral interface to the usb.

  cati_dataout_fifo_inst : entity work.cati_dataout_fifo
    port map (rst    => rst,
              wr_clk => clk_80Mhz,
              din    => main_fifo_datain,
              wr_en  => main_fifo_wr,
              full   => main_fifo_full,
              --
              rd_clk => fifo_rd_if.clk,
              rd_en  => fifo_rd_if.rd,
              dout   => fifo_rd_if.data,
              empty  => fifo_rd_if.empty
              );

end architecture Behavioral;
