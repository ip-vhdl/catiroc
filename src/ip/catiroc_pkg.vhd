-- * Common package

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- * Package

package catiroc_pkg is

  -- ** Constants

  constant RegBankLength : natural := 560;

  -- ** Generics

  constant c_nb_discri_channels : natural range 1 to 16 := 16;

  -- --! Architecture simple description

  -- ** Types

  -- *** Misc

  type t_nbasics_2bytes is array (natural range <>) of std_logic_vector(15 downto 0);

  -- *** Diff pair

  type rc_diff_pair is record
    p : std_logic;
    n : std_logic;
  end record rc_diff_pair;

  type rc_diff_pair_array is array (natural range <>) of rc_diff_pair;

  -- *** Catiroc

  type rc_catiroc_sc is record
    sr_clk    : std_logic;
    sr_in     : std_logic;              -- serialized data to configure cati
    sr_rst    : std_logic;  -- reset cati before sending configuration
    sr_select : std_logic;              -- to be able to write in cati
    sr_out    : std_logic;  -- serialized data to read back conf sen
  end record rc_catiroc_sc;

  type rc_catiroc is record
    cati_dataout : rc_diff_pair_array(1 downto 0);
    cati_strobe  : std_logic_vector (1 downto 0);
    sc           : rc_catiroc_sc;       -- slow control related
    val_evt      : rc_diff_pair;
    clk_40MHz    : rc_diff_pair;
    clk_160MHz   : rc_diff_pair;
    triggers     : std_logic_vector (15 downto 0);
    trig_ext     : std_logic;
    pwr_on       : std_logic;
    resetb       : std_logic;
    StartSys     : std_logic;
    ovf          : std_logic;
  end record rc_catiroc;

  type rc_catiroc_array is array (natural range <>) of rc_catiroc;

  -- *** Fifo if

  -- TODO:
  -- type t_fifo_if_data is array (natural range <>) of std_logic_vector;

  -- type t_fifo_if is record
  --   data  : t_fifo_if_data;
  --   empty : std_logic_vector;
  --   rd    : std_logic_vector;
  --   clk   : std_logic;
  -- end record t_fifo_if;

end catiroc_pkg;

package ft2232h_instance_pkg is new work.ft2232h_pkg
    generic map (p1_data_width   => 16,
                 p1_params_width => 4,
                 --
                 p2_data_width   => 8,
                 p2_params_width => 4,
                 --
                 p3_data_width   => 1,
                 p3_params_width => 66,
                 --
                 p4_data_width   => 16,
                 p4_params_width => 2,
                 --
                 p5_data_width   => 1,
                 p5_params_width => 3,
                 --
                 p6_data_width   => 16,
                 p6_params_width => 4,
                 --
                 RegBankLength   => work.catiroc_pkg.RegBankLength,
                 nb_peripherals  => 6);
