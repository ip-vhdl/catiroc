-- * Catiroc

-- * Libraries

library IEEE;
library osvvm;
context osvvm.OsvvmContext;

-- * Packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.catiroc_pkg.all;

-- * Entity

--! Mux entity brief description

--! Detailed description of this
--! mux design element.

entity catiroc_model is
  port (
    catirocs : inout rc_catiroc_array
    );
end entity catiroc_model;

-- * Architecture

--! @brief Architecture definition of the MUX
--! @details More details about this mux element.

architecture behavioral of catiroc_model is

  -- ** Signals

  -- type rc_catiroc_sc_array is array (natural range <>) of rc_catiroc_sc;

  -- signal sc : rc_catiroc_sc_array(catirocs'range);

  constant catimodel_id          : alertlogidtype := GetAlertLogID("catimodel_id");

begin

  -- ** Slow control

  u_sc : for i in catirocs'range generate

    -- sc(i) <= catirocs(i).sc;

    u_sc_i : process (catirocs(i).sc.sr_clk) is
    begin

      if (rising_edge(catirocs(i).sc.sr_clk)) then
        if (catirocs(i).sc.sr_rst = '0') then
          catirocs(i).sc.sr_out <= '0';
        else
          -- catirocs(i).sc.sr_select
          catirocs(i).sc.sr_out <= catirocs(i).sc.sr_in;
        end if;
      end if;

      end process u_sc_i;

  end generate u_sc;

  -- ** Data

  -- clk_40MHz_out_p(i) <= catirocs(i).clk_40MHz.p;
  -- clk_40MHz_out_n(i) <= catirocs(i).clk_40MHz.n;

  -- clk_160MHz_out_p(i) <= catirocs(i).clk_160MHz.p;
  -- clk_160MHz_out_n(i) <= catirocs(i).clk_160MHz.n;

  -- data capture

  -- catirocs(i).cati_strobe(i) <= cati_strobe(i);
  -- catirocs(i).cati_strobe(1) <= cati_strobe(1);

  -- catirocs(i).cati_dataout(i).p <= cati_dataout_p(i);
  -- catirocs(i).cati_dataout(i).n <= cati_dataout_n(i);

  -- catirocs(i).cati_dataout(1).p <= cati_dataout_p(1);
  -- catirocs(i).cati_dataout(1).n <= cati_dataout_n(1);

  -- ancillary

  -- catirocs(i).ovf <= ovf(i);
  -- pwr_on(i)       <= catirocs(i).pwr_on;
  -- resetb(i)       <= catirocs(i).resetb;
  -- StartSys(i)     <= catirocs(i).StartSys;

  -- val_evt_p(i) <= catirocs(i).val_evt.p;
  -- val_evt_n(i) <= catirocs(i).val_evt.n;

  -- triggering

  -- catirocs(i).triggers <= triggers;

end architecture behavioral;
