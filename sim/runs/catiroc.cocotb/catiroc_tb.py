#!/usr/bin/python

"""
Documentation.
"""

# TODO: replace random by https://github.com/mciepluc/cocotb-coverage

# * Imports

import os
import cocotb
from cocotb.triggers import Timer, FallingEdge, with_timeout, First, Lock
from cocotb.clock import Clock
from cocotb.binary import BinaryValue
from cocotb.utils import get_sim_time
from cocotb import fork
# from models.buses.usb.bus_usb import usb, WriteTransactionException, ReadTransactionException
from cocotb.result import SimTimeoutError, SimFailure, TestError, TestSuccess
# from testbench.testbench import tb_base
from ft2232h_tb import tb_ft2232h


# * Testbench class


class tb_catiroc(tb_ft2232h):
    """
    Testbench class. Implements infrastructure for testing dut.
    """

    # ** Init

    def __init__(self, dut):
        """
        Initialize things.
        """

        tb_ft2232h.__init__(self, dut)
        self.TEST_SLOW_CONTROL_NB_TIMES = int(os.environ['TEST_SLOW_CONTROL_NB_TIMES'])

        # initial reset
        # self.reset_task = fork(self.tb_base_reset())

    async def command_sc(self, params):
        """
        Send command CC with PARAMS.
        """

        self.tb_base_message("command slow control: sending command", 'debug')

        try:
            await self.usb.write(0xCC, params)
        except WriteTransactionException as e:
            self.tb_base_message("command CC: {:s}".format(e), 'error')
            raise TestError

        self.tb_base_message("command slow control: done command", 'debug')


# ** Tests

    # *** Slow control

    async def test_sc(self, nb_bytes):
        """
        Readback test.
        Sends a random array of NB_BYTES and reads it back again. Then, compares
        the two arrays.
        Raises a TestError exception in case of failure.
        """

        import random

        self.tb_base_message("test_sc: running test ...", 'info')

        # if nb_bytes > self.C_REGBANKLENGTH:
        #     raise ValueError
        # reference list to compare results with
        reference = [random.randrange(0, 255, 1) for i in range(nb_bytes)]
        # reference.insert(0, 5)
        # reference.append(255)
        # send command, blocks until complete
        # await self.command_sc(reference)
        # non blocking
        # import IPython
        # IPython.embed()
        fork(self.command_sc(reference))
        # Read data. See [[# ** Blocking write / read.][Blocking write/read]] note in ft2232h_tb.py module.
        # data = await self.usb.read(len(reference))
        # await FallingEdge(self.dut)
        databack = await self.usb.read(nb_bytes*3)
        data = databack[::3]
        asicnb = databack[1::3]
        boardid = databack[2::3]
        reference = reference[1:]
        if data[:nb_bytes-1] != reference[:nb_bytes-1]:
            raise TestError
        else:
            self.tb_base_message("test_sc: done test.", 'info')


# * Test slow control

@cocotb.test()
async def test_slow_control(dut):
    """
    Tests slow control.
    """

    tb = tb_catiroc(dut)

    tb.tb_base_message("Starting running slow control test", 'info')

    # Wait to start testing until reset task is over
    # tb.tb_base_message(await Join(tb.reset_task), 'debug')
    tb.tb_base_message("Waiting for init reset ...", 'debug')
    await FallingEdge(dut.rst)
    tb.tb_base_message("... initial reset is over", 'debug')
    await Timer(500, units='ns')

    for i in range(tb.TEST_SLOW_CONTROL_NB_TIMES):

        tb.tb_base_message("\tIteration {}".format(i), 'warning')

        task = fork(tb.test_sc(66))

        try:
            await with_timeout(task, tb.TIMEOUT, 'us')
        except SimTimeoutError as e:
            tb.tb_base_message("Exception SimTimeoutError %s" % (e), 'info')
            # raise TestError

    tb.tb_base_message("End running slow control test", 'info')

    raise TestSuccess


# * Test peripheral

@cocotb.test()
async def test_peripheral_p1(dut):
    """
    Tests peripheral.
    Just calls peripheral test once.
    """

    from ft2232h_tb import test_peripheral_p1
    await test_peripheral_p1(dut)

# * Test readback


@cocotb.test()
async def test_readback(dut):
    """
    Test readback.
    Performs readback test using a C_REGBANKLENGTH array, looping TEST_READBACK_NB_TIMES times.
    """

    from ft2232h_tb import test_readback
    await test_readback(dut)


# * Test dummy

@cocotb.test()
async def test_dummy(dut):
    """
    Tests dummy.
    Dummy test whose only purpose is force building the simulation executable.
    """

    from ft2232h_tb import test_dummy
    await test_dummy(dut)
